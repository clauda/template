// Namespacing Editor
var Editor = Editor || {};

// Keep editable text
Editor.txt = $('.textEditable');

Editor.bar = '#editor-bar';

var makeid = function()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

Editor.ing = null;

// Make all text editable
Editor.txt.attr('contentEditable', true);

$(Editor.txt).on('focus', function(e){
	var content = e.currentTarget;
	Editor.ing = makeid();
	$(content).addClass('editable').attr('id', Editor.ing);
	$(Editor.bar).show();
	$('#editor-bar').attr('data-target', '#' + Editor.ing);
	$('#' + Editor.ing).wysiwyg();
});

$('#closer').on('click', function(e){
	var content = e.currentTarget;
	$('#' + Editor.ing).removeClass('editable').removeAttr('id');
	$(Editor.bar).hide();
	Editor.ing = null;
});